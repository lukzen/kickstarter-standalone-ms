FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn clean package

ENV PORT 8080
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
HEALTHCHECK --start-period=40s --interval=5s --timeout=10s --retries=3 \
 CMD wget -q -s http://localhost:${PORT}/actuator/health || exit 1